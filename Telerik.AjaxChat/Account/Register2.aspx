﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register2.aspx.cs" Inherits="Telerik.AjaxChat.Account.Register2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
</head>
<body>
  <form id="form1" runat="server">
    <div>
      <asp:DynamicDataManager runat="server">
        <DataControls>
          <asp:DataControlReference ControlID="theForm" />
        </DataControls>
      </asp:DynamicDataManager>
      <asp:FormView runat="server" ID="theForm" DefaultMode="Insert" ItemType="Telerik.AjaxChat.Account.Register2+UserViewModel">
        <InsertItemTemplate>

          <asp:DynamicControl runat="server" Mode="Insert" ValidateRequestMode="Enabled" DataField="User" />

        </InsertItemTemplate>
      </asp:FormView>

    </div>
  </form>
</body>
</html>
