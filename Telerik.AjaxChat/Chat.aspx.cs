﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.AjaxChat.Hubs;
using Telerik.AjaxChat.Models;

namespace Telerik.AjaxChat
{
  public partial class Chat : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public IEnumerable<User> GetChatters()
    {
      return ChatHub._CurrentChatters.Select(k => new User { Name=k.Value, ConnectionId=k.Key }).ToArray();
    }
  
  }

}